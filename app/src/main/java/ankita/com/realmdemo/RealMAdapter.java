package ankita.com.realmdemo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealMAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final Context context;
    private ArrayList<UserInfo> al;

    public RealMAdapter(Context context, ArrayList<UserInfo> al) {
        this.al = al;
        this.context = context;
    }

    // create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
        return new ViewHolder(view);
    }

    // replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;
        holder.dbIdTv.setText(al.get(position).getDbId());
        holder.nameTv.setText(al.get(position).getUserName());
        holder.userIdTv.setText(al.get(position).getUserId());
        holder.emailTv.setText(al.get(position).getEmailId());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RealmResults<UserInfo> results = Realm.getDefaultInstance().where(UserInfo.class)
                        .equalTo("dbId", al.get(position).getDbId())
                        .findAll();
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                results.deleteAllFromRealm();
                realm.commitTransaction();
                ((MainActivity) context).loadData();
            }
        });
    }

    public int getItemCount() {
        return al.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView dbIdTv, nameTv, userIdTv, emailTv;
        LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            layout = (LinearLayout) view.findViewById(R.id.layout);
            dbIdTv = (TextView) view.findViewById(R.id.dbIdTv);
            nameTv = (TextView) view.findViewById(R.id.userNameTv);
            userIdTv = (TextView) view.findViewById(R.id.userIdTv);
            emailTv = (TextView) view.findViewById(R.id.emailIdTv);
        }
    }
}

package ankita.com.realmdemo;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<UserInfo> al = new ArrayList<>();
    Realm realm;
    EditText numberEt, userIdEt, nameEt, EmailEt;
    private RealMAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numberEt = (EditText) findViewById(R.id.numberEt);
        userIdEt = (EditText) findViewById(R.id.userIdEt);
        nameEt = (EditText) findViewById(R.id.nameEt);
        EmailEt = (EditText) findViewById(R.id.EmailEt);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        setAdapter();
        Realm.init(getApplicationContext());
        realm = Realm.getDefaultInstance();
        loadData();
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertData(numberEt.getText().toString());

            }
        });
    }
    private void expand(View v) {
        //set Visible


        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(widthSpec, heightSpec);

        ValueAnimator mAnimator = slideAnimator(0, v.getMeasuredHeight());
        mAnimator.start();
        v.setVisibility(View.VISIBLE);
    }
    private void collapse(View v) {
        int finalHeight = v.getHeight();

        ValueAnimator mAnimator = slideAnimator(finalHeight, 0);
        mAnimator.start();
        v.setVisibility(View.GONE);
    }
    private ValueAnimator slideAnimator(int start, int end) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        return animator;
    }

    private void setAdapter() {
        LinearLayoutManager ll = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(ll);
        adapter = new RealMAdapter(MainActivity.this, al);
        recyclerView.setAdapter(adapter);
    }

    public void insertData(String number) {
        if (!isValueExist(number)) {
            realm.beginTransaction();
            UserInfo userInfo = realm.createObject(UserInfo.class, number);
            userInfo.setUserName(nameEt.getText().toString());
            userInfo.setEmailId(EmailEt.getText().toString());
            userInfo.setUserId(userIdEt.getText().toString());
            realm.commitTransaction();
        } else {
            UserInfo userInfo = new UserInfo();
            userInfo.setUserName(nameEt.getText().toString());
            userInfo.setEmailId(EmailEt.getText().toString());
            userInfo.setUserId(userIdEt.getText().toString());
            userInfo.setDbId(number);
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(userInfo);
            realm.commitTransaction();
            Toast.makeText(this, "Record updated", Toast.LENGTH_SHORT).show();
        }
        loadData();
    }

    public void loadData() {
        RealmResults<UserInfo> result = realm.where(UserInfo.class)
                .findAllSorted("userName", Sort.ASCENDING);
        al.clear();
        for (int i = 0; i < result.size(); i++) {
            UserInfo ui = new UserInfo();
            ui.setDbId(result.get(i).getDbId());
            ui.setEmailId(result.get(i).getEmailId());
            ui.setUserName(result.get(i).getUserName());
            ui.setUserId(result.get(i).getUserId());
            al.add(ui);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // realm.close();
    }

    public boolean isValueExist(String id) {
        RealmResults<UserInfo> result = Realm.getDefaultInstance().where(UserInfo.class)
                .equalTo("dbId", id)
                .findAll();
        if (result.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
}

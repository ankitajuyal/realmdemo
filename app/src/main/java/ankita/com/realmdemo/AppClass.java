package ankita.com.realmdemo;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;

/**
 * Created by ipspl on 14/10/16.
 */

public class AppClass extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        Realm.init(this);
    }
}
